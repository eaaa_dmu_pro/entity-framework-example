﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntityFrameworkExample.Models
{
    public class AddPostModelView
    {
        public Blog Blog { get; set; }
        public Post Post { get; set; }
    }
}