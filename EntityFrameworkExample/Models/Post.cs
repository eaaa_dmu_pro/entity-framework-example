﻿namespace EntityFrameworkExample.Models
{
    public class Post
    {
        public int PostId { get; set; }
        public string Heading { get; set; }
        public string Content { get; set; }
        public Author Author { get; set; }
    }
}